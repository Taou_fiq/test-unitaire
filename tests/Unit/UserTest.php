<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    
    public function test_login_form()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }
    public function test_user_duplication()
    {
        $user1 = User::make([
            'name' => 'omar',
            'email' => 'omar@gmail.com'
        ]);
        $user2 = User::make([
            'name' => 'khalid',
            'email' => 'khalid@gmail.com'
        ]);
        $user3 = User::make([
            'name' => 'mhand',
            'email' => 'mhand@gmail.com'
        ]);
        $this->assertFalse($user1->name == $user2->name);
        $this->assertTrue($user1->name != $user2->name);
        
    }
}